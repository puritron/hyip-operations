.. HYIP Operations documentation master file, created by
   sphinx-quickstart on Sun Jan 15 21:05:46 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HYIP Operations's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: hyipops
   :members:

.. automodule:: hyip_parser
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
