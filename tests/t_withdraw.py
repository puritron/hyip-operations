#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import sys
import os
import logging
# import splinter
sys.path.append('..')
# import hyipops
import hyip_parser as hp

SAMPLES = "samples"
STATUS_PENDING = "Request pending"
STATUS_WITHDRAWN = r"Withdrawn\. Batch [0-9]+"


def balance_from_htm(file):
    htm = get_html(file)
    b = hp.get_balance(htm, 'perfectmoney')
    # print("{0}: {1}".format(file, b.get('perfectmoney')))
    return b


def get_html(file):
    with open(os.path.join(SAMPLES, file), "rb") as f:
        htm = f.read()
    return htm


class TestBalance(unittest.TestCase):

    def test_not_implemented(self):
        html = str()
        with self.assertRaises(NotImplementedError):
            hp.get_balance(html, script_type="DOGEPOWER!")

    def test_fail(self):
        # nothing found
        html = str()
        self.assertEqual(hp.get_balance(html, 'arse'), dict())
        # incorrect currency requested
        html = get_html('goldenbank_withdraw.htm')
        self.assertEqual(hp.get_balance(html, 'poodle'), None)

    def test_getGoldenbankBalance(self):
        self.assertEqual(
            balance_from_htm('goldenbank_withdraw.htm'),
            50.44)

    def test_getInstantCoin(self):
        self.assertEqual(
            balance_from_htm('instantcoin_withdraw.htm'),
            3.60)

    def test_getStablePower(self):
        self.assertEqual(
            balance_from_htm('stablepower_withdraw.htm'),
            54.00)

    def test_getDepositBonus(self):
        self.assertEqual(
            balance_from_htm('depositbonus_withdraw.htm'),
            3.12)

    def test_getAssetFinance(self):
        self.assertEqual(
            balance_from_htm('assetfinance_withdraw.htm'),
            4.00)

    def test_getNanoReturn(self):
        self.assertEqual(
            balance_from_htm('nanoreturn_withdraw.htm'),
            8.25)

    def test_getHourPay(self):
        self.assertEqual(
            balance_from_htm('hourpay_withdraw.htm'),
            2.31)

    def test_getMariaInv(self):
        self.assertEqual(
            balance_from_htm('mariainv_withdraw.htm'),
            1.49)

    def test_getBTCBerry(self):
        self.assertEqual(
            balance_from_htm('btcberry_withdraw.htm'),
            4.20)

    def test_getinvestmarketp(self):
        self.assertEqual(
            balance_from_htm('investmarketplace_withdraw.htm'),
            5.40)

    def test_zinc7(self):
        self.assertEqual(
            balance_from_htm('Zinc7_withdraw.htm'),
            3.50)

    def test_finfreedom(self):
        self.assertEqual(
            balance_from_htm('finfreedom_withdraw.htm'),
            3.90)

    def test_palmills(self):
        self.assertEqual(
            balance_from_htm('palmills_withdraw.htm'),
            0.35)

    def test_hourbitclub(self):
        self.assertEqual(
            balance_from_htm('hourbitclub_withdraw.htm'),
            0.00045)

    def test_hourbitclub(self):
        self.assertEqual(
            balance_from_htm('derivemoney_withdraw.htm'),
            1.98)


class Test_Get_EC(unittest.TestCase):

    # failure tests
    def test_fail(self):
        html = "<html></html>"
        self.assertEqual(hp.get_ec(html, script_type=hp.SCRIPT_GOLD), dict())

        self.assertEqual(
            hp.get_ec(html, 'arse', script_type=hp.SCRIPT_ZINC), dict())

    def test_not_implemented(self):
        html = str()
        with self.assertRaises(NotImplementedError):
            hp.get_ec(html, script_type="DOGEPOWER!")

    # success tests
    def test_nano(self):
        # Nanoreturn
        sample = {'perfectmoney': '18'}
        html = get_html('nanoreturn_withdraw.htm')
        result = hp.get_ec(html)
        self.assertEqual(sample, result)

    def test_palmills(self):
        sample = {'perfectmoney': '18'}
        html = get_html('palmills_withdraw.htm')
        result = hp.get_ec(html)
        self.assertEqual(sample, result)

    def test_imp(self):
        sample = {'perfectmoney': '18'}
        html = get_html('investmarketplace_withdraw.htm')
        result = hp.get_ec(html)
        self.assertEqual(sample, result)

    def test_hourbitclub(self):
        sample = {'perfectmoney': '18'}
        html = get_html('hourbitclub_withdraw.htm')
        result = hp.get_ec(html)
        self.assertEqual(sample, result)

    def test_zinc7(self):
        sample = {u'payza': u'payza',
                  u'perfectmoney': u'perfectmoney',
                  u'bitcoin': u'bitcoin',
                  u'payer': u'payer',
                  u'advcash': u'advcash'}
        html = get_html('Zinc7_withdraw.htm')
        result = hp.get_ec(html, script_type=hp.SCRIPT_ZINC)
        self.assertEqual(sample, result)

    def test_finfreedom(self):
        sample = {'perfectmoney': '18'}
        html = get_html('finfreedom_withdraw.htm')
        result = hp.get_ec(html)
        self.assertEqual(sample, result)

    def test_instantcoin(self):
        sample = {'perfectmoney': '2'}
        html = get_html('instantcoin_withdraw.htm')
        result = hp.get_ec(html)
        self.assertEqual(sample, result)


class TestPOSTRequests(unittest.TestCase):

    callback_result = list()

    def test_not_implemented(self):
        html = str()
        with self.assertRaises(NotImplementedError):
            hp.get_withdraw_post("DOGEPOWER!", html=html)

    def test_balance_less_amount(self):
        html = get_html('stablepower_withdraw_many.htm')
        sample = (({'a': 'withdraw',
                    'action': 'withdraw',
                    'amount': 14.74,
                    'comment': 'Your comment',
                    'ec': u'18'},
                   ('perfectmoney', 14.74)),)

        ret = hp.get_withdraw_post(hp.SCRIPT_GOLD, html, 'perfectmoney', 100.5)
        self.assertEqual(ret, sample)

    def test_balance_less_amount_zinc(self):
        html = get_html('zinc7_withdraw.htm')
        sample = (({'action': 'withdrawconfirm',
                    'pay_account': u'perfectmoney',
                    'amount': 3.5,
                    'transaction_code': None},
                   ('perfectmoney', 3.5)),)

        ret = hp.get_withdraw_post(hp.SCRIPT_ZINC, html, 'perfectmoney', 100.5)
        self.assertEqual(ret, sample)

    def test_zero_amount_zinc(self):
        html = get_html('zinc7_withdraw.htm')
        ret = hp.get_withdraw_post(hp.SCRIPT_ZINC, html, 'perfectmoney', 0.0)
        self.assertEqual(ret, list())

    def callback_function(self, wallet, amount):
        self.callback_result.append((wallet, amount))

    def test_callback(self):
        # check if callback is sane
        sample = [('perfectmoney', 14.74), ('bitcoin', 21.0)]
        html = get_html('stablepower_withdraw_many.htm')
        hp.get_withdraw_post(hp.SCRIPT_GOLD, html,
                             callback=self.callback_function)
        self.assertEqual(sample, self.callback_result)
        # test zinc
        sample = [('perfectmoney', 3.50)]
        self.callback_result = []
        html = get_html('zinc7_withdraw.htm')
        hp.get_withdraw_post(hp.SCRIPT_ZINC, html,
                             callback=self.callback_function)
        self.assertEqual(sample, self.callback_result)

    def test_type_1(self):
        # type 1:captcha
        sample = [({'a': 'withdraw',
                    'action': 'withdraw',
                    'amount': 14.74,
                    'comment': 'Your comment',
                    'ec': u'18'},
                   ('perfectmoney', 14.74)),
                  ({
                      'a': 'withdraw',
                      'action': 'withdraw',
                      'amount': 21.0,
                      'comment': 'Your comment',
                      'ec': u'48'},
                   ('bitcoin', 21.0))]
        html = get_html('stablepower_withdraw_many.htm')
        result = hp.get_withdraw_post(hp.SCRIPT_GOLD, html)
        self.assertEqual(result, sample)
        # type 1:pin
        sample = [({'a': 'withdraw',
                    'comment': 'Your comment',
                    'amount': 4.2,
                    'action': 'withdraw',
                    'transaction_code': '1234',
                    'ec': u'18'},
                   ('perfectmoney', 4.2))]
        html = get_html('btcberry_withdraw.htm')
        result = hp.get_withdraw_post(hp.SCRIPT_GOLD, html, pin='1234')
        self.assertEqual(result, sample)
        #
        sample = [({'a': 'withdraw',
                    'comment': 'Your comment',
                    'amount': 2.31,
                    'action': 'withdraw',
                    'transaction_code': '1234',
                    'ec': u'18'},
                   ('perfectmoney', 2.31))]
        html = get_html('hourpay_withdraw.htm')
        result = hp.get_withdraw_post(hp.SCRIPT_GOLD, html, pin='1234')
        self.assertEqual(result, sample)

        sample = [({'a': 'withdraw', 'comment': 'Your comment',
                    'amount': 5.4,
                    'action': 'withdraw',
                    'transaction_code': '1234',
                    'ec': u'18'},
                   ('perfectmoney', 5.4))]
        html = get_html('investmarketplace_withdraw.htm')
        result = hp.get_withdraw_post(hp.SCRIPT_GOLD, html, pin='1234')
        self.assertEqual(result, sample)

    def test_type_2(self):
                # type 2:pin
        sample = [({'a': 'withdraw',
                    'comment': '',
                    'Confirm': 'Confirm',
                    'ec': u'2',
                    'amount': 1.49,
                    'action': 'preview',
                    'transaction_code': '1234'},
                   ('perfectmoney', 1.49))]
        html = get_html('mariainv_withdraw.htm')
        result = hp.get_withdraw_post(hp.SCRIPT_GOLD, html, pin='1234')
        self.assertEqual(result, sample)

        sample = [({'a': 'withdraw',
                    'comment': '',
                    'Confirm': 'Confirm',
                    'ec': u'2',
                    'amount': 3.6,
                    'action': 'preview',
                    'transaction_code': '1234'},
                   ('perfectmoney', 3.6))]
        html = get_html('instantcoin_withdraw.htm')
        result = hp.get_withdraw_post(hp.SCRIPT_GOLD, html, pin='1234')
        self.assertEqual(result, sample)

    def test_zinc(self):
        sample = [({'action': 'withdrawconfirm',
                    'pay_account': u'perfectmoney',
                    'amount': 3.5,
                    'transaction_code': '1234'},
                   (u'perfectmoney', 3.5))]
        html = get_html('Zinc7_withdraw.htm')
        result = hp.get_withdraw_post(hp.SCRIPT_ZINC, html, pin='1234')
        self.assertEqual(sample, result)


class TestStatusChecker(unittest.TestCase):

    def test_not_implemented(self):
        with self.assertRaises(NotImplementedError):
            hp.check_status(str(), 'stinky')

    def test_pending(self):
        html = get_html('nanoreturn_request-saved.htm')
        self.assertEqual(hp.check_status(html, hp.SCRIPT_GOLD),
                         STATUS_PENDING)
        html = get_html('stablepower_request-saved.htm')
        self.assertEqual(hp.check_status(html, hp.SCRIPT_GOLD),
                         STATUS_PENDING)
        html = get_html('perfectbtc_pending.htm')
        self.assertEqual(hp.check_status(html, hp.SCRIPT_GOLD),
                         STATUS_PENDING)

    def test_processed_hourpay(self):
        html = get_html('hourpay_processed.htm')
        self.assertRegexpMatches(hp.check_status(html, hp.SCRIPT_GOLD),
                                 STATUS_WITHDRAWN)

    def test_processed_hourpower(self):
        html = get_html('hourpower_processed_302.htm')
        self.assertRegexpMatches(hp.check_status(html, hp.SCRIPT_GOLD),
                                 STATUS_WITHDRAWN)

    def test_processed_assetfinance(self):
        html = get_html('assetfinance_processed.htm')
        self.assertRegexpMatches(hp.check_status(html, hp.SCRIPT_GOLD),
                                 STATUS_WITHDRAWN)
        html = get_html('derivemoney_processed.htm')
        self.assertRegexpMatches(hp.check_status(html, hp.SCRIPT_GOLD),
                                 STATUS_WITHDRAWN)
        html = get_html('hourpower_processed.htm')
        self.assertRegexpMatches(hp.check_status(html, hp.SCRIPT_GOLD),
                                 STATUS_WITHDRAWN)
        html = get_html('mariainv_processed.htm')
        self.assertRegexpMatches(hp.check_status(html, hp.SCRIPT_GOLD),
                                 STATUS_WITHDRAWN)
        html = get_html('mariainv_withdrawn.htm')
        self.assertRegexpMatches(hp.check_status(html, hp.SCRIPT_GOLD),
                                 STATUS_WITHDRAWN)
        html = get_html('instantcoin_processed.htm')
        self.assertRegexpMatches(hp.check_status(html, hp.SCRIPT_GOLD),
                                 STATUS_WITHDRAWN)

    def test_processed_palmills(self):
        html = get_html('palmills_processed.htm')
        self.assertRegexpMatches(hp.check_status(html, hp.SCRIPT_GOLD),
                                 STATUS_WITHDRAWN)

        html = get_html('investmarketplace_pending.htm')
        self.assertRegexpMatches(hp.check_status(html, hp.SCRIPT_GOLD),
                                 STATUS_PENDING)

        html = get_html('hourbitclub_processed.htm')
        self.assertRegexpMatches(hp.check_status(html, hp.SCRIPT_GOLD),
                                 STATUS_WITHDRAWN)


class TestCaptcha(unittest.TestCase):

    def test_captcha_none(self):
        html = get_html('login/mariainv_login.htm')
        self.assertEqual(hp.get_captcha(html), None)

    def test_captcha_yes(self):
        html = get_html('login/assetfinance_login.htm')
        self.assertEqual(hp.get_captcha(html),
                         u'./assetfinance_login_files/saved_resource')

        html = get_html('login/stablepower_login.htm')
        img = (u'?a=show_validation_image&PHPSESSID='
               'fati2ljss8rf451ok3rdgsarb6&rand=546336978')
        self.assertEqual(hp.get_captcha(html),
                         img)
        html = get_html('login/investmp_login.htm')
        img = (u'./investmp_login_files/saved_resource(1)')
        self.assertEqual(hp.get_captcha(html),
                         img)

    def test_theory(self):
        html = '<input name="validation_number"/><img src="123">'
        img = "123"
        self.assertEqual(hp.get_captcha(html),
                         img)

    def test_fail(self):
        html = '<input name="validation_number"/>'
        with self.assertRaises(Exception):
            hp.get_captcha(html)


class TestLogin(unittest.TestCase):

    def test_perfectbtc(self):
        html = get_html('login/perfectbtc_login_ok.htm')
        result = hp.check_login_ok(html)
        self.assertTrue(result)

    def test_assetfinance(self):
        html = get_html('login/assetfinance_login_ok.htm')
        result = hp.check_login_ok(html)
        self.assertTrue(result)

    def test_zinc7(self):
        html = get_html('login/zinc7_login_ok.htm')
        result = hp.check_login_ok(html)
        self.assertTrue(result)

    def test_instantcoin(self):
        html = get_html('login/instantcoin_login_ok.htm')
        result = hp.check_login_ok(html)
        self.assertTrue(result)

    def test_finfreedom(self):
        html = get_html('login/financefreedom_login_ok.htm')
        result = hp.check_login_ok(html)
        self.assertTrue(result)

    def test_goldenbank(self):
        html = get_html('login/goldenbank_login_ok.htm')
        result = hp.check_login_ok(html)
        self.assertTrue(result)


class TestActiveBalance(unittest.TestCase):

    def test_biohourly(self):
        html = get_html('account/biohourly_account.htm')
        self.assertEqual(hp.get_active_deposit(html), 0.0)

    # def test_btcberry(self):
    #     html = get_html('account/btcberry_account.htm')
    #     self.assertEqual(hp.get_active_deposit(html), 38.0)

    def test_derivemoney(self):
        html = get_html('account/derivemoney_account.htm')
        self.assertEqual(hp.get_active_deposit(html), 20.0)

    def test_hourbitclub(self):
        html = get_html('account/hourbitclub_account.htm')
        self.assertEqual(hp.get_active_deposit(html), 0.015)

    def test_instantcoin(self):
        html = get_html('account/instantcoin_account.htm')
        self.assertEqual(hp.get_active_deposit(html), 50.0)

    def test_imp(self):
        html = get_html('account/investmarketplace_account.htm')
        self.assertEqual(hp.get_active_deposit(html), 40.0)


class TestHCert(unittest.TestCase):

    def test_bitaina(self):
        html = get_html('login/bitaina_login.htm')
        self.assertEqual(hp.H_get_cert(html), 'e7ecacf1')

        html = get_html('bitaina_withdraw.htm')
        self.assertEqual(hp.H_get_cert(html), '7a5b8a2f')

        html = get_html('bitaina_confirm.htm')
        self.assertEqual(hp.H_get_cert(html), 'f6830838')

        html = get_html('bitaina_pending.htm')
        self.assertEqual(hp.H_get_cert(html), '1dcef9d2')

    def test_trump(self):
        html = get_html('trump_processed.htm')
        self.assertEqual(hp.H_get_cert(html), 'dce56777')

    def test_bitaina(self):
        html = get_html('login/bitaina_login.htm')
        self.assertEqual(hp.H_get_cert(html), 'e7ecacf1')

        html = get_html('bitaina_withdraw.htm')
        self.assertEqual(hp.H_get_cert(html), '7a5b8a2f')

        html = get_html('bitaina_confirm.htm')
        self.assertEqual(hp.H_get_cert(html), 'f6830838')

        html = get_html('bitaina_pending.htm')
        self.assertEqual(hp.H_get_cert(html), '1dcef9d2')

    def test_trump(self):
        html = get_html('trump_processed.htm')
        self.assertEqual(hp.H_get_cert(html), 'dce56777')


def profile_this(test_class):
    """ run a specific tests for profile """
    # test_class = TestHCert
    suite = unittest.TestLoader().loadTestsFromTestCase(test_class)
    unittest.TextTestRunner().run(suite)


if __name__ == '__main__':
    logging.basicConfig(level=logging.CRITICAL)

    unittest.main()

    # import cProfile
    # cProfile.run('profile_this(TestStatusChecker)')
