#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
``hyipops``: HYIP Operations
============================

Automated HYIP Withdrawal using POST requests.

Scripts supported:

* Goldcoders

"""
import cfscrape
import hyip_parser as hp
import logging
from captcha_solver import CaptchaSolver
# import requests
# import Cookie


# def getHeaderCookies(header):
#     if header.get('Set-Cookie') is None:
#         return None
#     cook = Cookie.BaseCookie()
#     cook.load(header.get('Set-Cookie'))
#     morsels = cook.values()
#     # fix for non-int max-age cookie-attributes
#     for morsel in morsels:
#         if len(morsel.get('max-age', '')) > 0:
#             morsel['max-age'] = int(morsel['max-age'])
#     # end of fix
#     hdr_cookies = [requests.cookies.morsel_to_cookie(morsel)
#                    for morsel in morsels]
#     return hdr_cookies


def setSessionCookies(session, hdr_cookies):
    if hdr_cookies is None or len(hdr_cookies) == 0:
        return False
    for c in hdr_cookies:
        session.cookies.set_cookie(c)
    return True


class HYIPOps(object):
    """ HYIP Operations Class

    :param str url: HYIP main URL
    :param str login: username
    :param str password: password
    :param str pin: pincode
    :param str script: (optional) script name.
    :param bool nologin: (optional) do not login.
        :any:`login` should be called manually.

    Script is the HYIP engine. 95% of HYIPs are running
    on different versions of **goldcoders** script.

    Currently supported HYIP Scripts:

    * Goldcoders
    * Zinc
    """

    def __init__(self, url, login,
                 password, pin=None,
                 script=hp.SCRIPT_GOLD,
                 nologin=False,):
        self.url = url
        self.username = login
        self.password = password
        self.pin = pin
        # self.session = requests.Session()
        # self.session.headers['User-Agent'] = 'Mosaik'
        # CFScraper is more helpful
        self.session = cfscrape.create_scraper()
        if script is None:
            self.script_type = hp.SCRIPT_GOLD
        else:
            self.script_type = script
        self.elements = hp.load_elements(self.script_type)
        self.logger = logging.getLogger(__name__)
        self.response = None
        self.withdrawn = list()
        self.active_depo = None
        if not nologin:
            self.logged_in = self.login()

    def _check_captcha(self, html):
        """ Check if there's a captcha on the page and solve it

        :param str html: html of the login page

        """
        captcha_url = hp.get_captcha(html, self.script_type)
        solved_captcha = None
        if captcha_url is not None:
            solver = CaptchaSolver('browser')
            # Getting suspected captcha image
            request = self.session.get(self.url + captcha_url)
            if request.status_code != 200:
                raise Exception("Unable to get captcha image: "
                                "{0}.\nStatus code: {1}".format(
                                    self.url + captcha_url,
                                    request.status_code))
            raw_data = request.content
            print("Found CAPTCHA Image, it will be shown in a new window.")
            solved_captcha = solver.solve_captcha(raw_data)
        return solved_captcha

    def login(self):
        """Performs the login to HYIP

        .. todo::
          Currently supports only goldcoders, which is
          hardcoded. This should be changed.
        """
        self.logger.info("Logging in to {0}".format(self.url))

        url = self.url + self.elements['url_login']
        self.response = self.session.get(url)
        if self.response.status_code != 200:
            Exception("Unable to find login page. Server error: {0}"
                      .format(str(self.response.status_code)))

        html = self.response.content
        solved_captcha = self._check_captcha(html)
        # logging in
        post = hp.get_login_post(html, self.script_type,
                                 self.username, self.password, solved_captcha)
        self.response = self.session.post(url, data=post)
        # login status check
        if self.response.status_code != 200:
            raise Exception("Unable to login. Server error: {0}"
                            .format(str(self.response.status_code)))
        # checking login confirmation
        if not hp.check_login_ok(self.response.content):
            logging.debug(self.response.content)
            raise Exception("Failed to login. Check login details.")

        self.logger.info("Login successfull. Proceeding to accounts page")
        url = self.url + self.elements['url_account']
        self.response = self.session.get(url)
        if self.response.status_code != 200:
            raise Exception("Unable to get account page. "
                            "Server error: {0}"
                            .format(str(self.response.status_code)))
        self.active_depo = hp.get_active_deposit(self.response.content)

        return True


    def withdraw(self, ecurrency=None, amount=None):
        """Send withdraw requests to HYIP

        :param str ecurrency: (optional) ecurrency to withdraw
        :param str amount: (optional) amount to withdraw


        :return: list containing 3-tuples of the form
            ``(ecurrency,amount,status)``
        :rtype: list
        """
        url = self.url + self.elements['url_withdraw']
        self.response = self.session.get(url)
        if self.response.status_code != 200:
            raise Exception("Unable to fetch withdraw page. "
                            "Server error: {0}"
                            .format(str(self.response.status_code)))
        html = self.response.content
        if ecurrency is None:
            # figuring out what we can withdraw
            for payload in hp.get_withdraw_post(script_type=self.script_type,
                                                html=html,
                                                pin=self.pin):
                self.logger.debug("Payload: {0}".format(payload))
                post, data = payload
                # withdraw
                self.response = self.session.post(url, post)
                if self.response.status_code == 200:
                    result = hp.check_status(self.response.content,
                                             script_type=self.script_type)
                    w, a = data
                    self.withdrawn.append((w, a, result))
                else:
                    raise Exception("Withdrawal failed: Server error: {0}\n"
                                    "POST request: {1}"
                                    .format(
                                        self.response.status_code,
                                        post)
                                    )
        else:
            # TODO: finish this and optimize
            if ecurrency not in hp.VALID_CURRENCIES:
                raise ValueError("Invalid currency: {0}".format(ecurrency))
            payload = hp.get_withdraw_post(script_type=self.script_type,
                                           wallet=ecurrency, amount=amount,
                                           html=html, pin=self.pin)
            self.logger.debug("Payload: {0}".format(payload))
            post, data = payload

        return self.withdrawn

    def get_balance(self):
        url = self.url + self.elements['url_withdraw']
        self.response = self.session.get(url)
        return hp.get_balance(self.response.content,
                              script_type=self.script_type)

    def logout(self):
        if self.logged_in:
            if (self.session
                    .get(self.url + self.elements['url_logout'])
                    .status_code) != 200:
                raise Exception("Failed to logout.")
        return True

    def get_active_depo(self):
        """Returns current active deposit
        """
        pass


if __name__ == '__main__':
    pass
