# -*- coding: utf-8 -*-
"""
    Parser for Goldcoders script
"""
import re
from bs4 import BeautifulSoup, element

from .parser import HYIPParser


class Goldcoders(HYIPParser):
    """
    Parser for Goldcoders HYIP script
    """

    PARSER_NAME = 'goldcoders'

    # url suffixes
    URLS = {
        'account': '?a=account',
        'login': '?a=login',
        'logout': '?a=logout',
        'withdraw': '?a=withdraw',
    }

    # form names
    FORMS = {
        'login': 'mainform',
    }

    # fields
    FIELDS = {
        'captcha': 'validation_number',
        'username': 'username',
        'password': 'password',
        'pin': 'transaction_code',
    }

    def __init__(self, url):
        super(Goldcoders, self).__init__(url)

    #
    # Different checks
    #

    def check_status(self, html):
        REQUEST_WITHDRAWN = "Withdrawn. Batch {0}"
        REQUEST_PENDING = "Request pending"
        retval = None
        # check for pending

        if ('request saved' in html or
                'successfully saved' in html or
                "'0; URL=?a=withdraw&say=processed'" in html):
            # Withdrawal request saved
            retval = REQUEST_PENDING
        else:
            # Withdrawal processed. Batch id: 160180590 == OK
            batch = re.findall(r'Withdrawal[\w\s]*processed.*\s+([0-9]+)',
                               html)
            if batch:
                retval = REQUEST_WITHDRAWN.format(batch[0])
            else:
                batch = re.findall(r'withdraw&say=processed&batch=([0-9]+)',
                                   html)
                if batch:
                    retval = REQUEST_WITHDRAWN.format(batch[0])
        return retval

    def is_login_ok(self, html):
        """Checks if hyip shown the login success page

        :param str html: HTML of the reponse right after the login POST

        :return: True if login was successfull
        :rtype: bool
        """
        ok_re = [r"content=\".*url=\?a=account\">",
                 r"document\.location\.href='/account'"]
        retval = False
        for r in ok_re:
            if re.search(r, html, re.IGNORECASE):
                retval = True
                break
        return retval

    #
    # HYIP information
    #
    def get_active_deposit(self, html):
        """Get active deposit value

        :param str html: html of an account page (?a=account)

        :return: active deposit in HYIP's main currency.
        :rtype: float
        """
        f_depo = -1.0
        r = ur'active\s+deposit\s*:?[\s\n]+[$\u0243]([0-9.-]+)'
        soup = BeautifulSoup(html, self.HTML_PARSER)
        s_depo = re.search(r, soup.text, re.I + re.M)
        if s_depo:
            try:
                f_depo = float(s_depo.group(1))
            except:
                raise
                self._warning("Can't convert active depo {0} to float"
                              .format(s_depo))
        else:
            self._warning("Not found: Active Deposit")
        return f_depo

    def get_balance(self, html, wallet=None):
        """New and beautiful balance parser

        :param str html: html data from a withdraw web page
        :param str wallet: wallet to return balance for
        :param str script_type: (opt) determines which parser to use

        :return: if no wallet specified, returns dict.
            Returns float otherwise
        :rtype: dict or float

        :exception Exception: when balance cannot be determined
        """
        retval = dict()

        # \u0243 is a bitcoin symbol
        possible_re = (ur'([\w:]+)\s*[$\u0243]([-]?[0-9.]+)',
                       ur'\s*([\w: ]+)\n\s*[$\u0243]([-]?[0-9.]+)',)

        soup = BeautifulSoup(html, self.HTML_PARSER)

        form = soup.find('form')
        if form is None:
            return retval
        balance_tmp = re.findall(
            r'[$\u0243]([0-9.-]+)\sof\s([\w ]+)', form.text)
        balance = [(x[1], x[0].lower()) for x in balance_tmp]
        # try different combinations
        if len(balance) == 0:
            for r in possible_re:
                balance = re.findall(r, form.text)
                if len(balance) > 0:
                    break
        # drastic measures - search in soup.
        if len(balance) == 0:
            for r in possible_re:
                balance = re.findall(r, soup.text)
                if len(balance) > 0:
                    break

        all_balance = self._cleanup_balance({i[0]: float(i[1])
                                             for i in balance})

        if not wallet:
            retval = all_balance
        else:
            retval = all_balance.get(wallet,
                                     all_balance.get('balance',
                                                     None))
            if retval is None:
                self._warning("Unable to detect the balance. Wallet: '{1}'\n"
                              "Form data:\n{0}".format(form, wallet))

        return retval

    def get_ec(self, html, wallet=None):
        """ Gets ecurrency codes from withdraw page for POST requests

        Also this can be considered available wallets for withdrawal

        :param str html: html of the withdrawal page
        :param str wallet: (opt) currency type to get the code for
        :param str script_type: (opt) hyip script_type

        :return: currency code or dict with currencies if `wallet`
            is ``None``
        :rtype: str or dict
        """
        retval = None
        soup = BeautifulSoup(html, self.HTML_PARSER)
        try:
            start_tag = soup.find(lambda tag:
                                  tag.has_attr('name') and
                                  tag.attrs['name'] == 'ec')
            if start_tag.name == 'select':
                ec = start_tag.find_all('option')
                # form a ec mapping dict
                currency = {self._cleanup_ec(t.contents[0]): t.attrs['value']
                            for t in ec}
            elif start_tag.name == 'input':
                inputs = soup.find_all('input', {'name': 'ec'})
                currency = dict()
                for tag in inputs:
                    parents = tag.find_parents()
                    # finding parent tr tag
                    for t in parents:
                        if t.name == 'tr':
                            tr = t
                            break
                    # searching for currency namRe
                    for c in tr.contents:
                        if(isinstance(c, element.NavigableString)):
                            continue
                        currency_name = self._cleanup_ec(c.text)
                        if len(currency_name) == 0:
                            continue
                        else:
                            break
                    currency.update({currency_name: tag.attrs['value']})
            retval = currency if wallet is None else currency[wallet]
        except:
            retval = dict()
        if len(retval) == 0:
            self._warning(
                'get_ec:Failed to find ec values. Wrong '
                'page or withdraw form not available.')
        return retval

    def get_login_post(self, html,
                       captcha_code=None):
        post = {
            'a': 'do_login',
            'username': self.username,
            'password': self.password,
        }
        if captcha_code:
            post.update({self.FIELDS['captcha']: captcha_code, })

    def get_withdraw_post(self, html, wallet=None,
                          amount=None, pin=None, callback=None):
        """Get a sequence of POST requests for withdrawal

        Function returns a list for POST requests for the specified
        script_type, that should be issued sequentially in order to withdraw
        the specified amount of money from specified wallets.  If wallet and
        amount are not specified --- everything is withdrawn.

        :param str script_type: script_type to return POST requests for
        :param str html: withdrawal page html (in some cases used to
            determine what POST requests to return)
        :param str wallet: (opt) wallet to use, if not specified, assumed
            that all available wallets are to be withdrawn funds from
        :param str amount: (opt) amount to withdraw.  If not specified,
            post requests to withdraw all money from HYIP will be returned.
        :param str pin: pin code --- if any
        :param callable callback: callback function that will receive
            (wallet,amount) parameters for each wallet.


        :return: see explanation below.
        :rtype: list

        Return value logic:

        1. If no withdrawal available --- returns empty list
        2. If no wallets specified: `list`, comprised of
           `tuples` as described in (3)
        3. If wallet specified --- (post_request, (wallet,amount))
        """
        ec = self.get_ec(html)
        if ec is None or len(ec) == 0:
            # branch 1
            return list()
        if wallet is None:
            # branch 2
            retval = list()

            for w, code in ec.items():
                balance = self.get_balance(html, w,)

                post = self.get_withdraw_post(html,
                                              w, balance,
                                              pin, callback)[0]
                if post is not None:
                    retval.append(post)
        if wallet is not None:
            # branch 3
            balance = self.get_balance(html, wallet, )
            currency_code = self.get_ec(html, wallet, )
            if amount is None or float(amount) > float(balance):
                amount = balance
            if callback is not None and callable(callback):
                callback(wallet, amount)
            if 'apreview' in html:
                post = {
                    'a': 'withdraw',
                    'action': 'preview',
                    'amount': amount,
                    'ec': currency_code,
                    'comment': '',
                    'Confirm': 'Confirm',
                }
            else:
                post = {
                    'a': 'withdraw',
                    'action': 'withdraw',
                    'amount': amount,
                    'ec': currency_code,
                    'comment': 'Your comment',
                }
            if pin is not None:
                post.update({self.FIELDS['pin']: str(pin), })

            return ((post, (wallet, amount),),)

        return retval
