# -*- coding: utf-8 -*-
import re
import logging
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)


class HYIPParser(object):
    """Template class for HYIP parser

    :param str url: HYIP url
    """

    # this parser name. Change accordingly in subclass
    PARSER_NAME = 'template'

    # beautiful Soup html parser to use
    HTML_PARSER = 'html.parser'

    # url suffixes
    URLS = {
        'account': '',
        'login': '',
        'logout': '',
        'withdraw': '',
    }

    # form names
    FORMS = {
        'login': '',
    }

    # fields
    FIELDS = {
        'captcha': '',
        'username': '',
        'password': '',
        'pin': '',
    }

    VALID_BALANCES = ('payza',
                      'perfectmoney',
                      'bitcoin',
                      'payeer',
                      'advcash',
                      'balance')

    VALID_CURRENCIES = ('payza',
                        'perfectmoney',
                        'bitcoin',
                        'payeer',
                        'advcash',)

    def __init__(self, url, username, password):
        self.base_url = url
        self.username = username
        self.password = password

    #
    # Internal functions
    #

    def _cleanup_balance(self, balances):
        """Cleanup of ewallet balances

        1. Removes : and [space] from name and lower.
        2. Removes all entries, which key is not present in VALID_BALANCES

        :param dict balances: string containing wallet name
        :return: cleaned up balances
        :rtype: dict
        """
        retval = dict()

        for wallet, amount in balances.items():
            # mapping of wordings
            new_name = re.sub(r'[: ]+', '', wallet).lower()
            if new_name in self.VALID_BALANCES:
                retval[new_name] = amount

        return retval

    def _cleanup_ec(self, wallet):
        """Cleanup of ewallet names

        Removes : and [space] from name and lower.
        Also applies mapping

        :param str wallet: string containing wallet name

        :return: cleaned up name, or '' if the value is not in
            VALID_CURRENCIES
        """
        # mapping of wordings
        retval = ''
        value = re.sub(r'[: \n]+', '', wallet).lower()
        for entry in self.VALID_CURRENCIES:
            if entry in value:
                retval = entry
                break
        return retval

    def _warning(self, msg):
        """Used to output the warning messages
        """
        logger.warning(msg)

    #
    # Different checks
    #

    def check_status(self, html):
        pass

    def is_login_ok(self, html):
        pass

    #
    # HYIP information
    #
    def get_active_deposit(self, html):
        pass

    def get_balance(self, html, wallet=None):
        pass

    def get_captcha(self, html):
        """Generic CAPTCHA detector

        Get captcha image url from the page if present

        :param str html: html of the page
        :param str script_type: (optional) script type
            to load elements from

        :return: True if captcha present
        :rtype: bool
        """
        captcha_url = None
        if self.FIELDS['captcha'] in html:
            soup = BeautifulSoup(html, self.HTML_PARSER)
            captcha_entry = soup.find('input',
                                      {'name': self.FIELDS['captcha']})
            img = captcha_entry.find_previous('img')
            if img is None:
                img = captcha_entry.find_next('img')
                if img is None:
                    raise Exception(
                        "Captcha is somewhere but can't be captured (sorry)")
            captcha_url = img.attrs.get('src')

        return captcha_url

    def get_ec(self, html, wallet=None):
        pass

    def get_login_post(self, html,
                       captcha_code=None):
        pass

    def get_withdraw_post(self, html, wallet=None,
                          amount=None, pin=None, callback=None):
        pass
