#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
``hyip_parser``: Parsing functions
==================================

.. todo::

  * Implement check_status() parsing for Zinc 7
  * Detect active deposit and make a note to user.
  * ``check_status()`` value for palmills, pending for instantaction
"""
from bs4 import BeautifulSoup, element
import re
import logging
#from .parsers import *

SCRIPT_GOLD = 'goldcoders'
SCRIPT_ZINC = 'zinc7'
SCRIPT_H = 'H-Script'

VALID_BALANCES = ('payza',
                  'perfectmoney',
                  'bitcoin',
                  'payeer',
                  'advcash',
                  'balance')

VALID_CURRENCIES = ('payza',
                    'perfectmoney',
                    'bitcoin',
                    'payeer',
                    'advcash',)

HTML_PARSER = 'html.parser'


def load_elements(script_type):

    scripts = {
        SCRIPT_GOLD: {
            'form_login': 'mainform',
            'url_login': '?a=login',
            'url_account': '?a=account',
            'url_withdraw': '?a=withdraw',
            'url_logout': '?a=logout',
            'field_username': 'username',
            'field_password': 'password',
            'field_captcha': 'validation_number',
            'field_pin': 'transaction_code',
        },
        SCRIPT_ZINC: {
            'form_login': 'mainformlogin',
            'url_login': 'login',
            'url_account': 'account',
            'url_withdraw': 'withdraw',
            'url_logout': 'logout',
            'field_username': 'username',
            'field_password': 'password',
            'field_captcha': 'validation_number',
            'field_pin': 'transaction_code',
        },
        SCRIPT_H: {
            'url_login': 'login',
            'url_account': 'cabinet',
            'url_withdraw': 'operation?add=CASHOUT',

        }
    }
    if script_type is not None and script_type not in scripts.keys():
        raise ValueError("No such script type: {0}".format(script_type))
    return scripts[script_type]


def cleanup_balance(balances):
    """Cleanup of ewallet balances

    1. Removes : and [space] from name and lower.
    2. Removes all entries, which key is not present in VALID_BALANCES

    :param dict balances: string containing wallet name
    :return: cleaned up balances
    :rtype: dict
    """
    retval = dict()

    for wallet, amount in balances.items():
        # mapping of wordings
        new_name = re.sub(r'[: ]+', '', wallet).lower()
        if new_name in VALID_BALANCES:
            retval[new_name] = amount

    return retval


def cleanup_ec(wallet):
    """Cleanup of ewallet names

    Removes : and [space] from name and lower.
    Also applies mapping

    :param str wallet: string containing wallet name

    :return: cleaned up name, or '' if the value is not in
        VALID_CURRENCIES
    """
    # mapping of wordings
    retval = ''
    value = re.sub(r'[: \n]+', '', wallet).lower()
    for entry in VALID_CURRENCIES:
        if entry in value:
            retval = entry
            break
    return retval


def get_ec(html, wallet=None, script_type=SCRIPT_GOLD):
    """ Gets ecurrency codes from withdraw page for POST requests

    Also this can be considered available wallets for withdrawal

    :param str html: html of the withdrawal page
    :param str wallet: (opt) currency type to get the code for
    :param str script_type: (opt) hyip script_type

    :return: currency code or dict with currencies if `wallet`
        is ``None``
    :rtype: str or dict
    """
    retval = None
    logging.debug("Discovering available currencies for script {0}"
                  .format(script_type))
    soup = BeautifulSoup(html, HTML_PARSER)
    if script_type == SCRIPT_GOLD:
        try:
            start_tag = soup.find(lambda tag:
                                  tag.has_attr('name') and
                                  tag.attrs['name'] == 'ec')
            if start_tag.name == 'select':
                ec = start_tag.find_all('option')
                # form a ec mapping dict
                currency = {cleanup_ec(t.contents[0]): t.attrs['value']
                            for t in ec}
            elif start_tag.name == 'input':
                inputs = soup.find_all('input', {'name': 'ec'})
                currency = dict()
                for tag in inputs:
                    parents = tag.find_parents()
                    # finding parent tr tag
                    for t in parents:
                        if t.name == 'tr':
                            tr = t
                            break
                    # searching for currency name
                    for c in tr.contents:
                        if(isinstance(c, element.NavigableString)):
                            continue
                        currency_name = cleanup_ec(c.text)
                        if len(currency_name) == 0:
                            continue
                        else:
                            break
                    currency.update({currency_name: tag.attrs['value']})
            retval = currency if wallet is None else currency[wallet]
        except:
            retval = dict()

    elif script_type == SCRIPT_ZINC:
        try:
            li = soup.find_all(lambda tag: (tag.has_attr('data-value') and
                                            tag.name == 'li'))
            currency = {e.attrs['data-value']: e.attrs['data-value']
                        for e in li}

            retval = currency if wallet is None else currency[wallet]
        except:
            # if len(el) == 0:
            logging.error("get_ec: Could not detect currency with {0}.  "
                          "wallet = {1}".format(SCRIPT_ZINC, wallet))
            retval = dict()
    else:
        raise NotImplementedError(
            'Sorry, script_type not implemented: {0}'.format(script_type))
    if len(retval) == 0:
        logging.info(
            'get_ec:Failed to find ec values. Wrong '
            'page or withdraw form not available.')
    return retval


def get_balance(html, wallet=None, script_type=SCRIPT_GOLD):
    """New and beautiful balance parser

    :param str html: html data from a withdraw web page
    :param str wallet: wallet to return balance for
    :param str script_type: (opt) determines which parser to use

    :return: if no wallet specified, returns dict.
        Returns float otherwise
    :rtype: dict or float

    :exception Exception: when balance cannot be determined
    """
    retval = dict()

    possible_re = (ur'([\w:]+)\s*[$\u0243]([-]?[0-9.]+)',
                   ur'\s*([\w: ]+)\n\s*[$\u0243]([-]?[0-9.]+)',)

    soup = BeautifulSoup(html, HTML_PARSER)
    if script_type in (SCRIPT_GOLD, SCRIPT_ZINC,):
        form = soup.find('form')
        if form is None:
            return retval
        balance_tmp = re.findall(
            r'[$\u0243]([0-9.-]+)\sof\s([\w ]+)', form.text)
        balance = [(x[1], x[0].lower()) for x in balance_tmp]
        # try different combinations
        if len(balance) == 0:
            for r in possible_re:
                balance = re.findall(r, form.text)
                if len(balance) > 0:
                    break
        # drastic measures - search in soup.
        if len(balance) == 0:
            for r in possible_re:
                balance = re.findall(r, soup.text)
                if len(balance) > 0:
                    break

        all_balance = cleanup_balance({i[0]: float(i[1])
                                       for i in balance})

        if wallet is None:
            retval = all_balance
        else:
            retval = all_balance.get(wallet,
                                     all_balance.get('balance',
                                                     None))
            if retval is None:
                logging.error("Unable to detect the balance. Wallet: '{1}'\n"
                              "Form data:\n{0}".format(form, wallet))
    else:
        raise NotImplementedError(
            'Sorry, script_type not implemented: {0}'.format(script_type))
    return retval


def get_captcha(html, script_type=SCRIPT_GOLD):
    """Get captcha image url from the page if present

    :param str html: html of the page
    :param str script_type: (optional) script type
        to load elements from

    :return: True if captcha present
    :rtype: bool
    """
    captcha_url = None
    el = load_elements(script_type)
    if el['field_captcha'] in html:
        soup = BeautifulSoup(html, HTML_PARSER)
        captcha_entry = soup.find('input', {'name': 'validation_number'})
        img = captcha_entry.find_previous('img')
        if img is None:
            img = captcha_entry.find_next('img')
            if img is None:
                raise Exception("Captcha is somewhere but "
                                "can't be captured (sorry)")
        captcha_url = img.attrs['src']

    return captcha_url


def get_active_deposit(html, script_type=SCRIPT_GOLD):
    """Get active deposit value

    :param str html: html of an account page (?a=account)
    :param str script_type: (optional) script type
        to load elements from

    :return: active deposit in HYIP's main currency.
    :rtype: float
    """
    f_depo = -1.0
    r = ur'active\s+deposit\s*:?[\s\n]+[$\u0243]([0-9.-]+)'
    soup = BeautifulSoup(html, HTML_PARSER)
    s_depo = re.search(r, soup.text, re.I + re.M)
    if s_depo:
        try:
            f_depo = float(s_depo.group(1))
        except:
            raise
            logging.warning("Can't convert active depo {0} to float"
                            .format(s_depo))
    else:
        logging.warning("Not found: Active Deposit")
    return f_depo


def check_status(html, script_type=SCRIPT_GOLD):
    """Check the status of PerfectMoney withdrawal

    :param str html: HTML to parse

    :return: string with the result interpretation or
        `None` if something went wrong
    :rtype: str or None

    """
    REQUEST_WITHDRAWN = "Withdrawn. Batch {0}"
    REQUEST_PENDING = "Request pending"
    retval = None
    if script_type in (SCRIPT_GOLD, SCRIPT_ZINC):
        # check for pending

        if ('request saved' in html or
                'successfully saved' in html or
                "'0; URL=?a=withdraw&say=processed'" in html):
            # Withdrawal request saved
            retval = REQUEST_PENDING
        else:
            # Withdrawal processed. Batch id: 160180590 == OK
            batch = re.findall(r'Withdrawal[\w\s]*processed.*\s+([0-9]+)',
                               html)
            if batch:
                retval = REQUEST_WITHDRAWN.format(batch[0])
            else:
                batch = re.findall(r'withdraw&say=processed&batch=([0-9]+)',
                                   html)
                if batch:
                    retval = REQUEST_WITHDRAWN.format(batch[0])
    else:
        raise NotImplementedError(
            'Sorry, script_type not implemented: {0}'.format(script_type))
    return retval


def check_login_ok(html, script_type=SCRIPT_GOLD):
    """Checks if hyip shown the login success page

    :param str html: HTML of the reponse right after the login POST

    :return: True if login was successfull
    :rtype: bool
    """
    ok_re = [r"content=\".*url=\?a=account\">",
             r"document\.location\.href='/account'"]
    retval = False
    if script_type == SCRIPT_GOLD:
        for r in ok_re:
            if re.search(r, html, re.IGNORECASE) is not None:
                retval = True
                break
    return retval


def H_get_cert(html):
    """ Get _Cert parameter for H-Script

    :param str html: html of the login page
    """
    if '__cert' not in html.lower():
        return None
    # soup = BeautifulSoup(html, 'html.parser')
    # cert_tag = soup.find('input', {'name': '__Cert'})
    # retval = cert_tag.attrs['value']

    cert_re = r"<input\s+name=\"__Cert\"\s+value=\"(?P<cert>[a-fA-F0-9]+)\""
    match = re.search(cert_re, html)
    if match:
        retval = match.group(1)
    else:
        retval = None

    return retval


def get_login_post(html, script_type,
                   username, password,
                   captcha_code=None):
    elements = load_elements(script_type)
    post = dict()
    if script_type == SCRIPT_GOLD or script_type == SCRIPT_ZINC:
        post = {
            'a': 'do_login',
            'username': username,
            'password': password,
        }
        if captcha_code:
            post.update({elements['field_captcha']: captcha_code, })
    elif script_type == SCRIPT_H:
        post = {
            'Login': username,
            'Pass': password,
            'Remember': None,
            'URL': 'cabinet',
            '__Cert': H_get_cert(html),
            'login_frm_btn': 'Log+in',
        }
    else:
        raise NotImplementedError(
            'Sorry, script_type not implemented: {0}'.format(script_type))
    return post


def get_withdraw_post(script_type, html, wallet=None,
                      amount=None, pin=None, callback=None):
    """Get a sequence of POST requests for withdrawal

    Function returns a list for POST requests for the specified script_type,
    that should be issued sequentially in order to withdraw the specified
    amount of money from specified wallets.  If wallet and amount are not
    specified --- everything is withdrawn.

    :param str script_type: script_type to return POST requests for
    :param str html: withdrawal page html (in some cases used to
        determine what POST requests to return)
    :param str wallet: (opt) wallet to use, if not specified, assumed
        that all available wallets are to be withdrawn funds from
    :param str amount: (opt) amount to withdraw.  If not specified,
        post requests to withdraw all money from HYIP will be returned.
    :param str pin: pin code --- if any
    :param callable callback: callback function that will receive
        (wallet,amount) parameters for each wallet.


    :return: see explanation below.
    :rtype: list

    Return value logic:

    1. If no withdrawal available --- returns empty list
    2. If no wallets specified: `list`, comprised of
       `tuples` as described in (3)
    3. If wallet specified --- (post_request, (wallet,amount))
    """
    if script_type == SCRIPT_GOLD:
        ec = get_ec(html, script_type=script_type)
        if ec is None or len(ec) == 0:
            # branch 1
            return list()
        if wallet is None:
            # branch 2
            retval = list()

            for w, code in ec.items():
                balance = get_balance(html, w, script_type)

                post = get_withdraw_post(script_type, html,
                                         w, balance,
                                         pin, callback)[0]
                if post is not None:
                    retval.append(post)
        if wallet is not None:
            # branch 3
            balance = get_balance(html, wallet, script_type)
            currency_code = get_ec(html, wallet, script_type)
            if amount is None or float(amount) > float(balance):
                amount = balance
            if callback is not None and callable(callback):
                callback(wallet, amount)
            if 'apreview' in html:
                post = {
                    'a': 'withdraw',
                    'action': 'preview',
                    'amount': amount,
                    'ec': currency_code,
                    'comment': '',
                    'Confirm': 'Confirm',
                }
            else:
                post = {
                    'a': 'withdraw',
                    'action': 'withdraw',
                    'amount': amount,
                    'ec': currency_code,
                    'comment': 'Your comment',
                }
            if pin is not None:
                elements = load_elements(script_type)
                post.update({elements['field_pin']: str(pin), })

            return ((post, (wallet, amount),),)
    elif script_type == SCRIPT_ZINC:
        balances = get_balance(html, script_type=SCRIPT_ZINC)
        if balances is None or len(balances) == 0:
            # branch 1
            return list()
        if wallet is None:
            # branch 2
            retval = list()

            for w, balance in balances.items():
                if balance == 0.0 or w not in VALID_CURRENCIES:
                    continue
                post = get_withdraw_post(script_type, html,
                                         w, balance,
                                         pin, callback)[0]
                if post is not None:
                    retval.append(post)
        if wallet is not None:
            # branch 3
            if amount is not None and float(amount) == 0.0:
                return list()
            balance = get_balance(html, wallet, script_type)
            currency_code = get_ec(html, wallet, script_type)
            if amount is None or float(amount) > float(balance):
                amount = balance
            if callback is not None and callable(callback):
                callback(wallet, amount)
            post = {
                'action': 'withdrawconfirm',
                'amount': amount,
                'pay_account': currency_code,
                'transaction_code': pin,
            }
            return ((post, (wallet, amount),),)

    else:
        raise NotImplementedError(
            'Sorry, script_type not implemented: {0}'.format(script_type))

    return retval
