#!/usr/bin/env python
import sys
import logging
import datetime
import csv
if sys.version_info[0] > 2:
    import configparser as cp
    inp = input
else:
    import ConfigParser as cp
    inp = raw_input


DEFAULT_CONFIG = 'runner.cfg'


def usage(stream):
    """Print usage

    Print usage instructions to the stream

    :param handle stream: file handle. Can be sys.stderr for example.
        Must support write() method
    """
    stream.write("Usage: {0} [-a] [-h] [config file]\n\n"
                 "config file\n"
                 "        Configuration file containing HYIP details. If not\n"
                 "        specified, defaults to \"runner.cfg\"\n"
                 "-a\n"
                 "        withdraw from all hyips\n"
                 "-h\n"
                 "        this help screen\n".format(sys.argv[0]))


def make_csv_entry(hyip, result):
    # conn.logout()
    retval = list()
    if len(result) > 0:
        sys.stderr.write('OK\n')
        for r in result:
            if r[2] is None:
                code = 'Unknown'
            else:
                code = r[2]
            wallet = r[0] if r[0] != 'perfectmoney' else ''
            retval = [hyip['name'],
                      (datetime.datetime
                       .strftime(datetime.datetime.now(),
                                 '%d/%m/%Y %H:%M')),
                      '',
                      r[1],
                      '',
                      wallet + code
                      ]
    else:
        sys.stderr.write("Not withdrawn.\n")
    return retval


def withdraw(hyip):
    """Connects to hyip and performs a withdrawal
    """
    import hyipops

    sys.stderr.write("*** Withdrawing: {0} ... ".format(hyip['name']))
    result = []
    try:
        conn = hyipops.HYIPOps(url=hyip['url'],
                               login=hyip['username'],
                               password=hyip['password'],
                               pin=hyip['pin'],
                               script=hyip['script'])
        result = conn.withdraw()
    except KeyboardInterrupt:
        raise
    except:
        sys.stderr.write('Error: {0}\n'.format(sys.exc_info()[1]))
    logging.debug(result)
    return result


def load_hyips(conf_file):
    config = cp.ConfigParser()
    with open(conf_file, 'rb') as configfile:
        config.readfp(configfile)
    all_hyips = []
    for section in config.sections():
        hyip = {
            'name': section,
            'url': None,
            'username': None,
            'password': None,
            'pin': None,
            'script': None,
            'status': None,     # will be populated by withdrawer
        }
        hyip.update({k: v for k, v in config.items(section)})
        all_hyips.append(hyip)

    return sorted(all_hyips, key=lambda x: x['name'])


def print_menu(hyips):
    for i, h in enumerate(hyips):
        stat = "" if h['status'] is None else h['status']
        sys.stderr.write("[ {0:3} ] {1:20} [{2:.<2}] \n"
                         .format(i, h['name'], stat))


if __name__ == '__main__':

    config = DEFAULT_CONFIG
    process_all = False
    if len(sys.argv) > 1:
        for arg in sys.argv[1:]:
            if arg == '-h':
                usage(sys.stderr)
                sys.exit(0)
            if arg == '-a':
                process_all = True
                continue
            try:
                f = open(arg, "rb")
                f.close()
                config = arg
            except:
                sys.stderr.write("Error: File {0} not found.\n".format(arg))
                sys.exit(1)

    sys.stderr.write('Loading {0}\n'.format(config))
    hyips = load_hyips(config)
    # hyips = load_hyips('zinc7.cfg')

    results = list()
    results.append(['Name', 'Date', 'Deposit', 'Withdraw', '', 'Comment'])

    if process_all:
        for config in hyips:
            try:
                result = make_csv_entry(config, withdraw(config))
            except KeyboardInterrupt:
                sys.stderr.write("Aborted\n")
                break
            if len(result) == 0:
                config['status'] = 'NOT OK'
            else:
                config['status'] = 'OK'
                results.append(result)
    else:
        sys.stderr.write("Interactive mode\n"
                         "----------------\n")
        happy = True
        while True:
            print_menu(hyips)
            while True:
                sys.stderr.write(
                    "\nWhich HYIP to withdraw from?\nEnter number: ")
                try:
                    number = inp()
                    if len(number) == 0:
                        happy = False
                        break
                    try:
                        idx = int(number)
                        if (not hyips[idx] or
                                idx < 0 or idx > len(hyips)):
                            raise
                        break
                    except:
                        sys.stderr.write("Invalid number. Please retry.\n")
                        continue
                except (EOFError, KeyboardInterrupt) as err:
                    happy = False
                    break
            if not happy:
                break

            result = make_csv_entry(hyips[idx], withdraw(hyips[idx]))
            if len(result) == 0:
                hyips[idx]['status'] = 'NOT OK'
            else:
                hyips[idx]['status'] = 'OK'
                results.append(result)

    sys.stderr.write("\n")
    for h in hyips:
        if h['status'] is not None:
            sys.stderr.write("{0:20} : {1:6}".format(h['name'],
                                                     h['status'],
                                                     ))
            if (h['status'] == 'NOT OK'):
                sys.stderr.write(" --- {0:40}\n".format(h['url'],))
            else:
                sys.stderr.write("\n")

    if len(results) > 1:
        output = csv.writer(sys.stdout)
        output.writerows(results)
