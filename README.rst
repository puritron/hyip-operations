Automated HYIP Withdrawal
=========================

Quick setup
===========

Install the required packages::

  pip install -r requirements.txt

Copy ``runner.cfg.sample`` to ``runner.cfg`` and supply connection details::

  cp runner.cfg.sample runner.cfg

For interactive menu, start runner as::

  ./runner.py

For automated withdrawal::

  ./runner.py -a


Dev notes
=========
Parsers last updated in Feb 2017, and might be out of date.
